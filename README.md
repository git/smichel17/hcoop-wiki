# HCoop wiki v2

**What:** A from-scratch rebuild of the [hcoop wiki], organized using the [Divio documentation system].

**Why:** The current wiki has great information, but is not well-organized.. and MoinMoin does not lend itself to easy re-organization.

I wanted something git-backed, so I can work with the files directly. While hcoop has not yet chosen a [wiki replacement option], the beauty of a git-backed wiki is that I can start it here now and choose any of the git-backed options later.

**How:** As I personally find pieces of information to be useful, I will copy them over from the live wiki to here.

[hcoop wiki]: https://wiki.hcoop.net
[Divio documentation system]: https://documentation.divio.com/
[wiki replacement option]: https://wiki.hcoop.net/WikiReplacement2021
